﻿using UnityEngine;
using System.Collections;

public class followPlayer : MonoBehaviour 
{
	public Transform trif ;
	public float roteSpeed ;

	void Awake()
	{
		trif = GameObject.FindGameObjectWithTag("Player").transform ;
	}
	void Start()
	{
		roteSpeed = 1f ;
	}
	void Update () 
	{
		this.transform.position = trif.position ;
		camControl () ;
	}
	public void camControl()
	{
		if(Input.GetKey(KeyCode.UpArrow))
		{
			rotateCam(trif.right, roteSpeed) ;
		}
		if(Input.GetKey (KeyCode.LeftArrow))
		{
			rotateCam(trif.up, -roteSpeed) ;
			trif.Rotate(trif.up * -roteSpeed) ;
		}
		if(Input.GetKey (KeyCode.RightArrow))
		{
			rotateCam(trif.up, roteSpeed) ;
			trif.Rotate(trif.up * roteSpeed) ;
		}
		if(Input.GetKey (KeyCode.DownArrow))
		{
			rotateCam(trif.right, -roteSpeed) ;
		}
	}

	public void rotateCam(Vector3 axis, float speed)
	{
		transform.Rotate(axis * speed) ;
	}
}
