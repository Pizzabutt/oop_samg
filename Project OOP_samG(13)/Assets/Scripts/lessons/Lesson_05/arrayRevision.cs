﻿using UnityEngine;
using System.Collections;

public class arrayRevision : MonoBehaviour 
{
	//Vars
	public int[] collectiveAges ;
	public int ageCount ;
	public int currIndex ;
	public int indexLimit ;

	//Funcs
		//declareVars
	public void declareVars()
	{
		ageCount = Random.Range(0, 30) ;
		indexLimit = Random.Range (ageCount+1, 35) ;
	}
	public void updateVars()
	{
		collectiveAges = new int[ageCount] ;
		
	/*	for(int ix = 0 ; ix < ageCount ; ix++)
		{
			collectiveAges[ix] = ix+1 ;
		}
	*/
	}
		//stock
	void Start () 
	{
		declareVars () ;
	}
	void Update () 
	{
		updateVars() ;
		checkKeys() ;
	}
		//custom
	public void checkKeys()
	{
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			ageCount-- ;
			currIndex-- ;
			decreaseIndex() ;
		}
			
		if(Input.GetKeyDown (KeyCode.DownArrow))
		{
			ageCount++ ;
			currIndex++ ;
			increaseIndex () ;
		}
		
		if(Input.GetKeyDown(KeyCode.W))
		{
			setAge() ;
		}
	}
	
	public void decreaseIndex()
	{
		if(ageCount < 0)
		{
			ageCount = 0 ;
			currIndex++ ;
			Debug.LogWarning("Negative numbers cant be used as values for array size!") ;
		}
	}
	
	public void increaseIndex()
	{
		if(ageCount > indexLimit)
		{
			ageCount = indexLimit ;
			Debug.LogWarning("GOOD GOING GENIUS!") ;
		}
	}
	
	public void setAge()
	{
		for(int age = 0 ; age < ageCount ; age++)
		{
			collectiveAges[age] = 20 ;
		}
	}
}
