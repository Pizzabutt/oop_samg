﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct itemValues
{
	public string name ;
	public string description ;
	public string useMessage ;
}

public class inventory : MonoBehaviour 
{
	//Vars
	public itemValues[] items ;
	public int itemCount ;
	public int currItem ;
	private int ndx ; // ndx = index
	
	//Funcs
		//declareVars
	public void declareVars()
	{
		itemCount = 6 ;
		currItem = 0 ;
	}
	public void updateVars()
	{
		items = new itemValues[itemCount] ;
		
		for(ndx = 0 ; ndx < itemCount ; ndx++)
		{
			items[ndx].name = "Weapon " + ndx ;
			items[ndx].description = "Does thing " + ndx ;
			items[ndx].useMessage = "smacked the bitch" ;
		}
	}
		//stock
	void Start () 
	{
		declareVars() ;
		
	}
	void Update () 
	{
		updateVars () ;
		checkKeys () ;
		
	}
		//custom
	public void checkKeys()
	{
		if(Input.GetKeyDown(KeyCode.LeftBracket) && currItem > 0)
		{
			currItem-- ;
			print("using " + items[currItem].name) ;
		}
		
		if(Input.GetKeyDown(KeyCode.RightBracket) && currItem < itemCount-1)
		{
			currItem++ ;
			print("using " + items[currItem].name) ;
		}
		
		if(Input.GetKeyDown(KeyCode.Space))
		{
			print (items[currItem].useMessage) ;
		}
	}
}
