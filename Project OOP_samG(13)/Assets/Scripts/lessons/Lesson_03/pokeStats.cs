﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct stats
{
	public string pokeName ;
	public string type1 ;
	public string type2 ;
	public string atkType ;
	public string ability ;

	public int maxHP ;
	public int currHP ;
	
	public int atk ;
	public int def ;
	public int spAtk ;
	public int spDef ;
	public int speed ;
}

public class pokeStats : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
