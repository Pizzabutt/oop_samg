﻿using UnityEngine;
using System.Collections;

//Structs go here plz!!!!
[System.Serializable]
public struct Address
{
	public int streetNumber ;
	public string streetName ;
	public string suburb ;
	public int postCode ;
	public string province ;
	public string country ;
}

public class structExample : MonoBehaviour 
{
	public Address myAddress ;
	
	// Use this for initialization
	void Start () 
	{
		startingAddress () ; 
	}
	
	// Update is called once per frame
	void Update () 
	{
		newAddress () ;
	}

	public void startingAddress()
	{
		myAddress.streetNumber = 213 ;
		myAddress.streetName = "Pacific HighWay" ;
		myAddress.suburb = "St Leonards" ;
		myAddress.postCode = 2065 ;
		myAddress.province = "NSW" ;
		myAddress.country = "Australia" ;
	}

	public void newAddress()
	{
		if (Input.GetKey (KeyCode.Space)) 
		{
			myAddress.streetNumber = 123 ;
			myAddress.streetName = "Fake Street" ;
			myAddress.suburb = "Poopington" ;
			myAddress.postCode = 9001 ;
			myAddress.province = "???" ;
			myAddress.country = "Murica" ;
		}
	}
}
