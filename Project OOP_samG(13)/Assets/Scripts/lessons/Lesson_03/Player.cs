﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Stats
{
	public int health ;
	public int jumpForce ;
	public float score ;

	public float moveSpeed ;
	public float roteSpeed ;

	public string name ;

	public bool canJump ;

	public Controls controls ;
}

[System.Serializable]
public struct Controls
{
	public KeyCode keyForward ;
	public KeyCode keyLeft ;
	public KeyCode keyRight ;
	public KeyCode keyBack ;

	public KeyCode keyJump ;
}

public class Player : MonoBehaviour 
{
	//Vars
		//Structs
	public Stats stats ;

		//Components
	public Transform trif ;
	public Rigidbody reggie ;

	//Funcs
		//declare vars
	public void declareVars()
	{
		reggie = GetComponent<Rigidbody>() ;
		reggie.freezeRotation = true ;
		stats.health = 100 ;
		stats.moveSpeed = 20 ;
		stats.jumpForce = 150 ;
		stats.roteSpeed = 2 ;
		stats.name = "Tim" ;

		stats.controls.keyForward = KeyCode.W ;
		stats.controls.keyLeft = KeyCode.A ;
		stats.controls.keyRight = KeyCode.D ;
		stats.controls.keyBack = KeyCode.S ;

		stats.controls.keyJump = KeyCode.Space ;
	}
		//collisions
	void OnCollsionEnter(Collider other)
	{
		if (other.gameObject.tag == "ground") 
		{
			stats.canJump = true ;
		}
	}

		//stock
	void Start () 
	{
		declareVars () ;
	}
	void FixedUpdate () 
	{
		movement () ;
	}
		//Custom
			//movement
	public void movement()
	{
		wads () ;
		jump () ;
	}
	public void wads()
	{
				//move forward
		if (Input.GetKey (stats.controls.keyForward)) 
		{
			reggie.AddRelativeForce(Vector3.forward * stats.moveSpeed) ;
		}
				//move left
		if (Input.GetKey (stats.controls.keyLeft)) 
		{
			reggie.AddRelativeForce(Vector3.left * stats.moveSpeed) ;
		}
				//move right
		if(Input.GetKey(stats.controls.keyRight))
		{
			reggie.AddRelativeForce(Vector3.right * stats.moveSpeed) ;
		}
				//move back
		if(Input.GetKey(stats.controls.keyBack))
		{
			reggie.AddRelativeForce(Vector3.back * stats.moveSpeed) ;
		}
	}
			//jump
	public void jump()
	{
		if(Input.GetKeyDown(stats.controls.keyJump) && stats.canJump == true)
		{
			reggie.AddForce(Vector3.up * stats.jumpForce) ;
		}
	}
}
