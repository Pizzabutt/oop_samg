﻿using UnityEngine;
using System.Collections;

public class classModifier : MonoBehaviour 
{
	//Vars
	public classPractice otherClass ;

	//Funcs
	void Start () 
	{
		otherClass = GetComponent<classPractice>() ;

		otherClass.printNumber () ;
		otherClass.printFloat () ;
		otherClass.printString () ;
		otherClass.printOtherWord () ;
		otherClass.booboo () ;
	}
	
	void Update () 
	{
	
	}
}
