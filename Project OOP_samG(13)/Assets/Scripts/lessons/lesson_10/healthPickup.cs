﻿using UnityEngine;
using System.Collections;

public class healthPickup : MonoBehaviour 
{
	//Vars
	public int healAmount ;

	//Funcs
	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
	void OnTriggerEnter(Collider other)
	{
		Player triggerPlayer = other.gameObject.GetComponent<Player>() ;

		if(triggerPlayer != null)
		{
			addHealth (triggerPlayer) ;
			destroyObject () ;
		}
	}

	private void destroyObject()
	{
		Destroy (gameObject) ;
	}

	private void addHealth(Player targetPlayer)
	{
		targetPlayer.stats.health += healAmount ;
	}

}
