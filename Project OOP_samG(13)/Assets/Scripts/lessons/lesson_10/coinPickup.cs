﻿using UnityEngine;
using System.Collections;

public class coinPickup : MonoBehaviour 
{
	public int coinValue ;
	private int spinSpeed ;

	// Use this for initialization
	void Start () 
	{
		coinValue = 5 ;
		spinSpeed = 5 ;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(spinSpeed,0,0) ;
	}

	void OnTriggerEnter(Collider other)
	{
		Player player = other.GetComponent<Player>() ;

		if(player != null)
		{
			addCoin(player) ;
			destroyCoin () ;	
		}
	}

	public void addCoin(Player target)
	{
		target.stats.score += coinValue ;
	}
	public void destroyCoin()
	{
		Destroy (gameObject) ;
	}
}
