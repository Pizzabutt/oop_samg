﻿using UnityEngine;
using System.Collections;

public class inputTutorial : MonoBehaviour 
{
	//Vars
		//ints

		//floats

		//bools

		//strings
	public string keypressed ;
	public string wasPressed ;

	//Funcs
		//StUpdate
	void Start () 
	{
		wasPressed = " was pressed!" ;
	}
	void Update () 
	{
		checkKeys () ;
	}

		//Custom
	void checkKeys()
	{
		if (Input.GetKey (KeyCode.W)) 
		{
			keypressed = "W" ;
			print (keypressed + wasPressed) ;
		}
		if (Input.GetKey (KeyCode.A)) 
		{
			keypressed = "A" ;
			print (keypressed + wasPressed) ;

		}
		if (Input.GetKey (KeyCode.S)) 
		{
			keypressed = "S" ;
			print (keypressed + wasPressed) ;
		}
		if (Input.GetKey (KeyCode.D)) 
		{
			keypressed = "D" ;
			print (keypressed + wasPressed) ;
		}
	}
}
