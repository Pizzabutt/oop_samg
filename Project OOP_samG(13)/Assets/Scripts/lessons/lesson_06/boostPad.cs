﻿using UnityEngine;
using System.Collections;

public class boostPad : MonoBehaviour 
{
	//Vars
	public Rigidbody playerReggie ;
	public float force ;

	//Funcs
		//decvars

		//Stock
	void OnTriggerStay(Collider other)
	{
		if(other.tag == "Player")
		{
			playerReggie = other.GetComponent<Rigidbody>() ;
			applyForce (this.transform.forward, force) ;
		}
	}

	void Start () 
	{
		force = 1f ;
	}
		//Custom
	public void applyForce(Vector3 direction, float power)
	{
		playerReggie.AddForce(direction * power, ForceMode.Impulse) ;
	}
}
