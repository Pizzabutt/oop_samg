﻿using UnityEngine;
using System.Collections;

public class functionsAgain : MonoBehaviour 
{

	//Vars
	public string myName ;
	public int myAge ;
	public string myGender ;

	private int result ;
	public int operand1 ;
	public int operand2 ;

	//Funcs
		//decVars
	public void startVars()
	{
		setDetails("Bob", 12, "Poop") ;
	}
		//Stock
	void Start () 
	{
		startVars() ;
	}
	void Update () 
	{
		checkKeys (KeyCode.KeypadPlus, KeyCode.KeypadMinus, KeyCode.KeypadMultiply, KeyCode.KeypadDivide) ;
	}
		//Custom
	public void setName(string name)
	{
		myName = name ;
	//	print (myName) ;
	}
	public void setAge(int age)
	{
		myAge = age ;
	//	print (myAge) ;
	}
	public void setGender(string gender)
	{
		myGender = gender ;
	//	print (myGender) ;
	}
	public void setDetails(string name, int age, string gender)
	{
		setName(name) ;
		setAge(age) ;
		setGender(gender) ;
	}

	public void checkKeys(KeyCode plus, KeyCode minus, KeyCode multiply, KeyCode divide)
	{
		if(Input.GetKeyDown (plus))
		{
			addNumbers (operand1, operand2) ;
		}
		if(Input.GetKeyDown (minus))
		{
			subtractNumbers (operand1, operand2) ;
		}
		if(Input.GetKeyDown(multiply))
		{
			multiplyNumbers (operand1, operand2) ;
		}
		if(Input.GetKeyDown(divide))
		{
			divideNumbers (operand1, operand2) ;
		}
	}
	public void subtractNumbers(int operand1, int operand2)
	{
		result = operand1 - operand2 ;
		print (result) ;
	}
	public void addNumbers(int operand1, int operand2)
	{
		result = operand1 + operand2 ;
		print (result) ;
	}
	public void multiplyNumbers(int operand1, int operand2)
	{
		result = operand1 * operand2 ;
		print (result) ;
	}
	public void divideNumbers(int operand1, int operand2)
	{
		result = operand1 / operand2 ;
		print (result) ;
	}
}
