﻿using UnityEngine;
using System.Collections;

public class movingObjects : MonoBehaviour 
{
	//Vars
	private Rigidbody reggie ;

	public float moveSpeed ;

	//Funcs
		//decvars
	public void startVars()
	{
		reggie = GetComponent<Rigidbody>() ;
		moveSpeed = 10f ;
	}
		//Stock
	void Start () 
	{
		startVars () ;
	}
	void Update () 
	{
		checkKeys (KeyCode.W, KeyCode.A, KeyCode.D, KeyCode.S, KeyCode.Space) ;
	}
		//Custom
	public void checkKeys(KeyCode forward, KeyCode left, KeyCode right, KeyCode back, KeyCode jump)
	{
		if(Input.GetKey(forward))
		{
			move(Vector3.forward, moveSpeed, ForceMode.Force) ;
		}
		if(Input.GetKey(left))
		{
			move(Vector3.left, moveSpeed, ForceMode.Force) ;
		}
		if(Input.GetKey(right))
		{
			move(Vector3.right, moveSpeed, ForceMode.Force) ;
		}
		if(Input.GetKey(back))
		{
			move(Vector3.back, moveSpeed, ForceMode.Force) ;
		}
		if(Input.GetKeyDown(jump))
		{
			move (Vector3.up, moveSpeed * 50f, ForceMode.Impulse) ;
		}
	}
	private void move(Vector3 direction, float forceAmount, ForceMode forceMode)
	{
		reggie.AddForce(direction * forceAmount) ;
	}
}
