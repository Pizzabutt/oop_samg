﻿using UnityEngine;
using System.Collections;

public class scopeModifier : MonoBehaviour 
{
	//Vars
	public scopeTutorial otherClass ;
	public string messageCopy ;

	//Funcs
		//Stock
	void Start () 
	{
		otherClass.scopeFunction() ;
	}
	void Update () 
	{
		if(Input.GetKeyDown (KeyCode.Return))
		{
			otherClass.printSecretMessage () ;
		}
		if(Input.GetKeyDown (KeyCode.Backspace))
		{
			otherClass.changeSecretMessage ("gettin spoopy") ;
		}
		if(Input.GetKeyDown (KeyCode.Backslash))
		{
			messageCopy = otherClass.getSecretMessage () ;
		}
	}

		//Custom
}
