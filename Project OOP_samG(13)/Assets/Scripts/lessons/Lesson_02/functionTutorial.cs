﻿using UnityEngine;
using System.Collections;

public class functionTutorial : MonoBehaviour 
{
	//Vars
		//components

		//ints
	public int myAge ;

		//floats
	public float moveSpeed ;
	private float runSpeed ;
	private float walkSpeed ;

	public float rotSpeed ;

		//strings
	public string myFirstName ;
	public string myGender ;
	public string randomSentence ;

		//bools
	public bool isSprinting ;
	//Funcs
		//Stock
	void Start () 
	{
		rotSpeed = 3f ;

		runSpeed = 10f ;
		walkSpeed = 5f ;
		moveSpeed = walkSpeed ;

		myFirstName = "sam" ;
		myAge = 17 ;
		myGender = "male" ;
		randomSentence = "sweet poop bicycle on a 60's film detectives' catch phrase" ;

		printMyName () ;
		printID () ;
	}
	void Update () 
	{

	}
	void FixedUpdate()
	{
		checkKeys () ;
	}
	
		//Custom
	public void printMyName()
	{
		print ("sam") ;
	}
	public void printID()
	{
		print (myFirstName) ;
		print (myAge) ;
		print (myGender) ;
		print (randomSentence) ;
	}
		//PlayerMovement
	public void checkKeys()
	{
			//Move forward
		if (Input.GetKey (KeyCode.W)) 
		{
			playerMoveForward() ;
		}
			//Move left
		if (Input.GetKey (KeyCode.A)) 
		{
			playerMoveLeft() ;
		}
			//Move right
		if (Input.GetKey (KeyCode.D)) 
		{
			playerMoveRight() ;
		}
			//MoveBack
		if (Input.GetKey (KeyCode.S)) 
		{
			playerMoveBack() ;
		}
			//Sprint turn on
		if (Input.GetKeyDown (KeyCode.LeftShift)) 
		{
			isSprinting = true ;
			playerCheckSprint() ;
		}
			//Sprint turn off
		if (Input.GetKeyUp (KeyCode.LeftShift)) 
		{
			isSprinting = false ;
			playerCheckSprint() ;
		}

			//Rotate left
		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			transform.Rotate(Vector3.down * rotSpeed) ;
		}
			//Rotate right
		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			transform.Rotate(Vector3.up * rotSpeed) ;
		}
	}
		//Player move funcs
	public void playerMoveForward()
	{
		transform.Translate (Vector3.forward * moveSpeed) ;
		print ("w key pressed") ;
	}
	public void playerMoveLeft()
	{
		transform.Translate (Vector3.left * moveSpeed) ;
		print ("a key pressed") ;
	}
	public void playerMoveRight()
	{
		transform.Translate (Vector3.right * moveSpeed) ;
		print ("d key pressed") ;
	}
	public void playerMoveBack()
	{
		transform.Translate (Vector3.back * moveSpeed) ;
		print ("s key pressed") ;
	}
	public void playerCheckSprint()
	{
		if (isSprinting == true) 
		{
			moveSpeed = runSpeed;
		} 
		else 
		{
			moveSpeed = walkSpeed;
		}
	}
}



