﻿using UnityEngine;
using System.Collections;

public class arrayTutorial : MonoBehaviour 
{

	//Vars
		//ints
	public int[] add1 ;

		//floats

		//Stings
	public string[] studentNames ;

	//Funcs
		//DeclareVars
	public void declareVars()
	{
		studentNames = new string[20] ;
		add1 = new int[studentNames.Length] ;
	}
		//Stock
	void Start () 
	{
		declareVars () ;

		setNames () ;
	//	printNames () ;
	}
	void Update ()
	{
		//testLoop () ;
	}
		//Custom
	private void setNames()
	{
	//	studentNames[0] = "adopted" ;
	//	studentNames[1] = "French" ;
	//	studentNames[2] = "Assface" ;

		for(int i = 0; i < studentNames.Length; i++)
		{
			add1[i] = i + 1 ;
			studentNames[i] = "Orphan No. " + Random.Range(add1[i], studentNames.Length) ;
		}
	}

/*	public void printNames()
	{
		for(int i = 0; i < studentNames.Length; i++)
		{
			print (studentNames[i]) ;
		}
	}
/*
	public void testLoop()
	{
		for(int index = 0; index < 10; index++)
		{
			print(index) ;		
		}
	}
*/
}
