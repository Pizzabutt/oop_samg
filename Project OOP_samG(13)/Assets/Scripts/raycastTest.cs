﻿using UnityEngine;
using System.Collections;

public class raycastTest : MonoBehaviour 
{
	//Vars
	public Transform trif ;
	private Vector3 direc ; //direction
	public float range ;

	//Funcs
		//decvars
	public void startVars()
	{
		trif = GetComponent<Transform>() ;
		range = 10f ;
	}
		//Stock
	void Start () 
	{
		startVars () ;
	}
	void Update () 
	{
		direc = trif.forward ;
		shoot(trif.position, direc, range) ;
	}
		//Custom
	public void shoot(Vector3 position, Vector3 direction, float range)
	{
		RaycastHit hit ;
		Ray hitScan = new Ray (position, direction) ;
		Debug.DrawRay (position, direction * range) ;
		
		if(Input.GetKeyDown (KeyCode.Space)
		&& Physics.Raycast (hitScan, out hit, range)
		&& hit.collider.tag == "enemy")
		{
			Destroy(hit.collider.gameObject) ;
			print ("killed a dude") ;
		}
	}
}
