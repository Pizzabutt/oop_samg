﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct keyBinds
{
	public KeyCode keyForward ;
	public KeyCode keyLeft ;
	public KeyCode keyRight ;
	public KeyCode keyBack ;
	
	public KeyCode keySprint ;
	public KeyCode keyJump ;
}

[System.Serializable]
public struct playerMove
{
	public int runSpeed ;
	public int walkSpeed ;
	public int roteSpeed ; //"Rote" = rotation

	public float moveSpeed ;

	public string printMoveSpeed ;

	public bool isSprinting ;
	public bool holdingKeyForward ;
}

public class RigidBodyMover : MonoBehaviour 
{
	//Vars
		//structs
	public keyBinds controls ;
	public playerMove moving ;

		//components
	private Rigidbody reggie ; //will always refer to rigidbody as "reggie"
	private Transform trif ; //will always refer to transform as "trif"

	//Funcs
		//declareVars
	public void declareVars()
	{
		reggie = GetComponent<Rigidbody> () ;
		trif = GetComponent<Transform> () ;

		controls.keyForward = KeyCode.W ;
		controls.keyLeft = KeyCode.A ;
		controls.keyRight = KeyCode.D ;
		controls.keyBack = KeyCode.S ;

		controls.keySprint = KeyCode.LeftShift ;
		controls.keyJump = KeyCode.Space ;
		
		moving.roteSpeed = 2 ;
		moving.runSpeed = 75 ;
		moving.walkSpeed = 50 ;
		moving.moveSpeed = moving.walkSpeed ;
	}

		//Stock
	void Start ()
	{
		declareVars () ;
	}
	void FixedUpdate () 
	{
		moving.printMoveSpeed = moving.moveSpeed + " Move Speed" ;

		playerMovement () ;
	}

		//Custom Funcs
			//Movement
	public void playerMovement()
	{
		wads () ;
		playerLook () ;
	}
			//WADS
	public void wads()
	{
				//sprint on
		if (Input.GetKeyDown (controls.keySprint) && moving.holdingKeyForward == true) 
		{
			moving.isSprinting = true ;
			playerSprint() ; 
		}
				//sprint off
		if (Input.GetKeyUp (controls.keySprint)) 
		{
			moving.isSprinting = false ;
			playerSprint() ;
		}
		
				//Move Forward
		if (Input.GetKey (controls.keyForward)) 
		{
			reggie.AddRelativeForce (Vector3.forward * moving.moveSpeed);
			moving.holdingKeyForward = true;
		} 
		else 
		{
			moving.holdingKeyForward = false ;
		}
				//Move Left
		if (Input.GetKey (controls.keyLeft)) 
		{
			reggie.AddRelativeForce(Vector3.left * moving.moveSpeed) ;
		}
				//Move Right
		if (Input.GetKey (controls.keyRight)) 
		{
			reggie.AddRelativeForce(Vector3.right * moving.moveSpeed) ;
		}
				//Move Back
		if (Input.GetKey (controls.keyBack)) 
		{
			reggie.AddRelativeForce(Vector3.back * moving.moveSpeed) ;
		}
	}
			//Rotate + look Left/Right
	public void playerLook()
	{
		float yRote = Input.GetAxisRaw ("Mouse X") * moving.roteSpeed ;
		trif.Rotate (0, yRote, 0) ;
	}
			//sprinting
	public void playerSprint()
	{
		if (moving.isSprinting == true) 
		{
			moving.moveSpeed = moving.runSpeed ;
		} 
		else 
		{
			moving.moveSpeed = moving.walkSpeed ;
		}
	}
}