﻿using UnityEngine;
using System.Collections;

public class clockTest : MonoBehaviour 
{
	//Vars
		//ints
	private int seconds ;
	private int minutes ;

		//floats
	private float frames ;

		//bools

		//strings
	public string printClock ;

	//Funcs
		//Stock
	void Start () 
	{
		minutes = 0 ;
		seconds = 0 ;
		frames = 0 ;
	}
	void FixedUpdate()
	{
		frames = frames + 1 ;
		checkTime () ;
	}
		//Custom
	/*IEnumerator clearConsole()
	{
		while (!Debug.developerConsoleVisible) 
		{
			yield return null ;
		}
		yield return null ;
		Debug.ClearDeveloperConsole() ;
	}*/

		//Custom
	void checkTime()
	{
		//every 60 frames adds a second to the clock
		if (frames >= 60)
		{
			frames = 0 ;
			seconds = seconds + 1 ;

			clock () ;
		}
		
		if (seconds >= 60) 
		{
			seconds = 0;
			minutes = minutes + 1;

			clock () ;
		}
	}
	void clock()
	{
		//prints the seconds and minutes
		printClock = minutes + " : " + seconds + " . " + frames ;
	}
}

















