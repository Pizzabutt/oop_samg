﻿using UnityEngine;
using System.Collections;

public class randomizer : MonoBehaviour 
{
	/*
	* this script generates a number randomly using the Vector class which return as floats
	* depending on which 2 values it is between it will provide a bonus effect to the player (print to console)
	* i should use an array of floats if i want more than 4 possible results
	*/

	//Vars
		//components
	public Vector4 randomSet ;
	public float randomReturn ;

	//Funcs
		//declareVars
	public void declareVars()
	{
		randomSet.x = 100f ; //always highest value
		randomSet.y = 25f ; // always lowest value
		randomSet.z = 50f ; // for percentage chances z is cutoff
		randomSet.w = 75f ; //misc vaule
	}
	public void updateVars()
	{
		randomReturn = Random.Range (0f, randomSet.x) ;
	}

		//Stock
	void Start () 
	{
		declareVars () ;
	}
	void Update () 
	{
		updateVars () ;
		randomValues () ;
	}

		//Custom
	public void randomValues()
	{
		if(Input.GetKeyDown(KeyCode.A))
		{
			if(randomReturn < randomSet.y)
			{
				print ("DOUBLE DAMAGE!") ;
			}

			if(randomReturn > randomSet.y && randomReturn < randomSet.z)
			{
				print ("DOUBLE SCORE!") ;
			}

			if(randomReturn > randomSet.z && randomReturn < randomSet.w)
			{
				print ("IMMUNITY!") ;
			}

			if(randomReturn > randomSet.w)
			{
				print ("DOUBLE SPEED!") ;
			}
		}
	}
}
