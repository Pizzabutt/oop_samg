﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerPlanetBehaviour : MonoBehaviour 
{
	/*
	 * This script goes onto the top parent object of the player.
	 * This script pushes the player to the world and rotates it so it is always standing up
	 * on the sphere, relative to its position.
	 */ 

	private Rigidbody rb;
	private Vector3 dirToPlanet;
	public float gravity;
	public Transform groundTarget;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	void Update()
	{
		dirToPlanet = (groundTarget.position - transform.position).normalized;

		alignToPlanet();
	}

	void FixedUpdate()
	{
		pullToPlanet();
	}

	private void alignToPlanet()
	{
		transform.rotation = Quaternion.FromToRotation(transform.up, -dirToPlanet) * transform.rotation;
	}

	private void pullToPlanet()
	{
		rb.AddForce (dirToPlanet * gravity);
	}
}