﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct keyControls
{
	public KeyCode[] wads ;
}

public class movement : MonoBehaviour 
{
	//Vars
		//Scripts
	public gravityTest gravCheck ;

		//Structs
	public keyControls keyBinds ;

		//Components
	public Rigidbody reggie ;
	public Transform[] trif ;

		//floats
	private float moveSpeed ;
	public float jumpForce ;

	private float roteSpeed ;
	public float xRote ;
	public float yRote ;

	//Funcs
		//declare vars
	public void delcareVars()
	{
		gravCheck = GetComponent<gravityTest> () ;

		reggie = GetComponent<Rigidbody> () ;

		moveSpeed = 1 ;
		jumpForce = 100 ;
		roteSpeed = 2 ;
	}

		//Stock
	void Start () 
	{
		delcareVars () ;
	}
	void Update () 
	{
		playerMovement () ;
	}

		//Custom
			//movement
	public void playerMovement()
	{
	//	findSpeed () ;

		coreMovement () ;

		if(gravCheck.aligned == true)
		{
			inGravLook() ;
		/*
			for(int i = 0; i < 1; i++)
			{
				trif [1].rotation = trif [3].rotation ;
			}
		*/
		}

		if(gravCheck.aligned == false)
		{
			outGravLook() ;
		}
	}
	public void coreMovement()
	{
		if(Input.GetKey(keyBinds.wads[0]))
		{
			reggie.MovePosition(Vector3.forward * moveSpeed) ;
		}

		if(Input.GetKey(keyBinds.wads[1]))
		{
			trif[0].Translate(Vector3.left * moveSpeed) ;
		}

		if(Input.GetKey(keyBinds.wads[2]))
		{
			trif[0].Translate(Vector3.right * moveSpeed) ;
		}

		if(Input.GetKey(keyBinds.wads[3]))
		{
			trif[0].Translate(Vector3.back * moveSpeed) ;
		}

		if(Input.GetKey(keyBinds.wads[4]))
		{
			reggie.AddRelativeForce(Vector3.up * jumpForce) ;
		}

	}
				//look
	public void inGravLook()
	{
		xRote = Input.GetAxisRaw ("Mouse Y") * roteSpeed ;
		yRote = Input.GetAxisRaw ("Mouse X") * roteSpeed ;

		trif [1].Rotate (-xRote, 0, 0) ;
		trif [0].Rotate (0, yRote, 0) ;

		print("In Gravity") ;
	}
	public void outGravLook()
	{
		xRote = Input.GetAxisRaw ("Mouse Y") * roteSpeed ;
		yRote = Input.GetAxisRaw ("Mouse X") * roteSpeed ;

		trif [0].Rotate (-xRote, yRote, 0) ;
		trif [1].rotation = trif[2].rotation ;

		print("Out of Gravity") ;
	}
	
}





