﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class gravityTest : MonoBehaviour 
{
	//Vars
		//Components
	public Transform[] gravFinder ;
	public Transform currGravTarget ;
	private Transform trif ;

	private Vector3 gravAlign ;
	private Rigidbody reggie ;

		//floats
	public float gravityStrength ;

		//bools
	public bool inGrav ;
	public bool aligned ;

	//Funcs
		//Declare Vars
	public void declareVars()
	{
		trif = GetComponent<Transform> () ;
		reggie = GetComponent<Rigidbody> () ;
	}

		//Stock
	void OnTriggerStay(Collider other)
	{

		if(other.gameObject.tag == "aligner")
		{
			alignment() ;
		}

		if(other.gameObject.tag == "planet")
		{
			currGravTarget = other.transform ;
			inGrav = true ;
			trif.parent = other.transform ;
		}
	}
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "planet")
		{
			currGravTarget = null ;
			inGrav = false ;
		}
	}

	void Start () 
	{
		declareVars () ;
	}
	void Update () 
	{
		gravAlign = (currGravTarget.position - trif.position).normalized ;

		gravPull () ;

	}

		//Custom
	public void gravPull()
	{
		reggie.AddForce (gravAlign * gravityStrength) ;
	}
	public void alignment()
	{
		trif.rotation = Quaternion.FromToRotation (trif.up, -gravAlign) * trif.rotation ;
		aligned = true ;
	}
}




