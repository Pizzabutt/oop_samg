﻿using UnityEngine;
using System.Collections;

public class spawnPlatform : MonoBehaviour 
{
	//Vars
	public Vector3 offset ;
	public GameObject platform ;

	private Rigidbody playerReggie ;
	public float speedRO ;
	public Vector2 offsetRange ;

	//Funcs
	public void offsetBySpeed ()
	{
		offset.x = 50f + Random.Range (speedRO - offsetRange.x, speedRO + offsetRange.x) ;
		offset.y = Random.Range(0f - offsetRange.y, 0f + offsetRange.y) ;
	}

	public void spawn()
	{
		Instantiate(platform, transform.position + offset, Quaternion.identity) ;
		Destroy (this.gameObject) ;
	}
}
