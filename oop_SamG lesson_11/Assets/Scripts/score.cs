﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class score : MonoBehaviour 
{
	//Vars
	private Vector3 spawnPoint = Vector3.zero ;
	public float playScore ;
	public Text scoreText ;

	//Funcs
	void Start () 
	{
		playScore = 0f ;
	}
	
	void Update () 
	{
		playScore = Mathf.Round(Vector3.Distance (spawnPoint, transform.position)) ;
		scoreText.text = "Score: " + playScore ;
	}
}
