﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class playerMove : MonoBehaviour 
{
	//Vars
	public float maxSpeed ;
	private float speed ;
	public float jumpForce ;
	private Rigidbody reggie ;

	private spawnPlatform gay ;
	public string tagHazard ;
	public string tagSpawner ;
	public string tagRespawn ;
	//Funcs
	void Start () 
	{
		reggie = GetComponent<Rigidbody>() ;
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == tagHazard)
		{
			speed = 0f ;
			Destroy (other.gameObject) ;
		}
		if(other.gameObject.tag == tagSpawner)
		{
			gay = other.GetComponent<spawnPlatform>() ;
			gay.speedRO = reggie.velocity.magnitude ;
			gay.offsetBySpeed() ;
			gay.spawn() ;
		}
		if(other.gameObject.tag == tagRespawn)
		{
			Application.LoadLevel (Application.loadedLevel) ;
		}
	}
	void Update () 
	{
		speed += 0.0006f ;
		if(speed >= maxSpeed)
		{
			speed = maxSpeed ;
		}
		transform.Translate (transform.right * speed) ;
		if(Input.GetKey(KeyCode.Space))
		{
			reggie.AddRelativeForce(Vector3.up * jumpForce, ForceMode.Impulse) ;
		}
	}
}
