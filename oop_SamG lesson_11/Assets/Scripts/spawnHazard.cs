﻿using UnityEngine;
using System.Collections;

public class spawnHazard : MonoBehaviour 
{
	//Vars
	public float spawnChance ;
	public GameObject hazard ;

	private Vector3 spawnPos ;
	//Funcs
	void Start () 
	{
		determineOutcome (spawnChance) ;
	}
	void Update () 
	{
		
	}
	
	public void determineOutcome(float chance)
	{
		float value ;
		value = Random.Range (0f, 100f) ;
		if(value < chance)
		{
			spawn () ;
		}
	}

	public void spawn()
	{
		spawnPos = new Vector3(Random.Range (transform.position.x - 24f,transform.position.x + 20f), transform.position.y + 1f, 0) ;
		Instantiate (hazard, spawnPos, Quaternion.identity) ;
	}
}
